# Qt Installer Framework
Installer for [streamCapture2](https://gitlab.com/posktomten/streamcapture2)<br><br>
The Qt Installer Framework provides a set of tools and utilities to create installers for the supported desktop Qt platforms: Linux, Microsoft Windows, and OS X.<br>
[https://doc.qt.io/qtinstallerframework/](https://doc.qt.io/qtinstallerframework/)<br>
