21:24 September 16, 2019
Version 0.14.0
Removed question marks from search address.
Uses Qt 5.13.1, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, compiled with GCC 5.4.0 (Linux).

19:53 September 16, 2019
Version 0.13.9
Updated svtplay-dl version 2.4-20-g0826b8f (Linux and Windows).
Uses Qt 5.13.1, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, compiled with GCC 5.4.0 (Linux).

23:24 September 13, 2019
Version 0.13.8
Updated svtplay-dl version 2.4-6-gce9bfd3 (Linux and Windows).
Uses Qt 5.13.1, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, compiled with GCC 5.4.0 (Linux).

21:59 September  11, 2019
Version 0.13.7
Updated svtplay-dl version 2.4-2-g5466853 (Linux and Windows).
Uses Qt 5.13.1, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, compiled with GCC 5.4.0 (Linux).

09:19 September  7, 2019
Version 0.13.6
Updated svtplay-dl version 2.3-23-gbc15c69 (Linux and Windows).
Updated ffmpeg version 4.2 (Windows).
Uses Qt 5.13.1, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, compiled with GCC 5.4.0 (Linux).

22:17 September  2, 2019
Version 0.13.5
Updated svtplay-dl version 2.2-5-gd1904b2 (Linux).
Updated svtplay-dl version 2.2-7-g62cbf8a (Windows).
Clearer code and comments.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

00:06 September  1, 2019
Version 0.13.4
Updated svtplay-dl version 2.2-2-g838b3ec (Linux).
Updated svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Minor bug. Don't show "Search..." after unsuccessful search.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

18:03  August  28, 2019
Version 0.13.3
Updated svtplay-dl (version 2.2-1-g9146d0d, Linux and Windows).
Download link points to gitlab.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

09:48  August  24, 2019
Version 0.13.2
Updated svtplay-dl (version 2.1-65-gb64dbf3, Linux and Windows).
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

17:43  August  21, 2019
Version 0.13.1
Bug fix: "Automatically checks for updates when the program starts" now works.
Updated svtplay-dl (version 2.1-57-gf429cfc, Linux and Windows).
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

17:59  August  17, 2019
Version 0.13.0
Information about what is updated when the program checks for updates.
Improved "About".
Moved "Password" from "File" menu to "Pay TV".
Better status messages.
Confirmation that the video stream has been downloaded
Display numbers of downloaded streams.
Operating system dependent display of path.
Minor bug fix.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

12:07  August  6, 2019
Version 0.12.0
Updated svtplay-dl (version 2.1-56-gbe6005e, Windows).
Downgraded ffmpeg (Windows, version 4.1.4, stable).
Create automatic folders, if needed. (Same video stream in different qualities).
Ability to view more information from svtplay-dl.
Add status-tip text.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux)

16:32 July 31, 2019
Version 0.11.0
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux)

22:41 July 30, 2019
Version 0.11.0 RC 1
Improved help and manual.
Editable Download list.
Possibility to save the same stream in different sizes.
Ability to enter user name and password. For paid streaming services.
Replace obsolete code with more modern.
Use QSettings.
Several bug fixes.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux)

14:39 July 5, 2019
Version 0.10.7
Several bug fixes. Works better to download a single file.
Duplicate in the selection of different video quality removed.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

14:59 June 29, 2019
Version 0.10.5
Updated svtplay-dl (version 2.1-53-gd33186e).
Updated ffmpeg (Windows, version N-94129-g098ab93257).
Architecture instruction set 64-bit.
Uses Qt 5.13.0, compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, compiled with GCC 5.4.0 (Linux).

21:29 February 13, 2018
Version 0.10.3
Updated svtplay-dl (version 1.9.7).
Architecture instruction set 64-bit.
Uses Qt 5.10.0, compiled with MSVC++ 15.5 (MSVS 2017).

15:41 August 9, 2017
Version 0.10.1
Check box to search for, load and insert subtitles.
in the video file.
Dynamic user interface.
Updated ffmpeg (version 3.3.3).
Architecture instruction set 64-bit.
Uses Qt 5.9.1, compiled with MSVC++ 15.0 (MSVS 2017).

01:19 July 26, 2017
Version 0.10.0
Combo Box to choose quality.
Architecture instruction set 64-bit.
Uses Qt 5.9.1, compiled with MSVC++ 15.0 (MSVS 2017).

15:48 July 16, 2017
Version 0.9.4
Updated ffmpeg (version 3.3.2)
Architecture instruction set 64-bit.
Uses Qt 5.9.1, compiled with MSVC++ 15.0 (MSVS 2017).

09:38 May 7, 2017
Version 0.9.3
Updated svtplay-dl (version 1.9.4).
Updated ffmpeg (version 3.2.4).
Display Version history

16:33 January 27, 2017
Version 0.9.2
Updated svtplay-dl (version 1.9.1).

18:00 January 14, 2017
Version 0.9.1
Updated svtplay-dl and ffmpeg.
German translation.
Minor improvments.

01:55 October 30, 2016
Release version 0.9.0
